package com.example.thiago.empresas_android.presentation.Company

import com.example.thiago.empresas_android.domain.entities.Company

interface CompanyCardView {
    fun setCompanyName(companyName : String)

    fun setCompanyCountry(companyCountry : String)

    fun setCompanyDescription(companyDescription : String)

    fun setCompanyType(companyType : String)

    fun setOnClickListener(company: Company)
}
