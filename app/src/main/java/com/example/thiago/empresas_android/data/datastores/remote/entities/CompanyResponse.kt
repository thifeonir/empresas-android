package com.example.thiago.empresas_android.data.datastores.remote.entities

import com.google.gson.annotations.SerializedName

data class CompanyResponse (@SerializedName("enterprise_name")
                       val name: String,
                       @SerializedName("description")
                       val description: String,
                       @SerializedName("country")
                       val country: String,
                       @SerializedName("enterprise_type")
                       val enterpriseType: Enterprise)