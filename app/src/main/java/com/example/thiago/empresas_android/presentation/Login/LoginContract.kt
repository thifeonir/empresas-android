package com.example.thiago.empresas_android.Login

import com.example.thiago.empresas_android.data.datastores.remote.entities.UserResponse

interface LoginContract {

    interface View {

        fun validateLogin(email : String, password : String)

        fun showLoading()

        fun hideLoading()

        fun showLoginButton()

        fun hideLoginButton()

        fun showToast(message : String)

        fun startCompanyActivity()

        fun saveUserInfo(userInfo : UserResponse)
    }

    interface Presenter {
        fun doLogin(email : String, password : String)

        fun attachView(view : LoginContract.View)
    }
}