package com.example.thiago.empresas_android.data.datastores.remote.entities

import com.google.gson.annotations.SerializedName

class Enterprise (@SerializedName ("enterprise_type_name") val enterpriseTypeName : String)