package com.example.thiago.empresas_android.Company

import android.util.Log
import com.example.thiago.empresas_android.data.datastores.remote.ServiceApi
import com.example.thiago.empresas_android.data.datastores.remote.entities.CompanyResponse
import com.example.thiago.empresas_android.data.datastores.remote.entities.UserResponse
import com.example.thiago.empresas_android.data.datastores.remote.mappers.CompanyMapper
import com.example.thiago.empresas_android.domain.entities.Company
import com.example.thiago.empresas_android.presentation.Company.CompanyCardView
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.ArrayList

class CompanyPresenter : CompanyContract.Presenter {

    private lateinit var companyView : CompanyContract.View
    private val serviceApi = ServiceApi
    private val companyMapper = CompanyMapper()

    private var companiesList : List<Company> = emptyList()
    private var companiesCache : List<Company> = emptyList()


    override fun attachView(view: CompanyContract.View) {
        companyView = view
    }

    override fun loadCompanies(userInfo : UserResponse) {

        serviceApi.provideApiService()
                .getCompanies(userInfo.uid,
                userInfo.accessToken,
                userInfo.client)
                .subscribeOn(Schedulers.io())
                .map {response: JsonObject -> getCompanies(response)}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    companiesCache = it.map(companyMapper::transform)
                    setCompanyRecycler(it.map(companyMapper::transform))
                },{
                    Log.d("Errorrr", it.localizedMessage)
                },{
                    Log.d("Completed", "complete")
                })

    }

    override fun getCompanyRowCount(): Int {
        return companiesList.size
    }

    override fun notifyDataChanged() {
        companyView.onDataChanged()
    }

    override fun onBindCompanyRowViewAtPosition(position: Int, view: CompanyCardView) {

        companiesList[position].apply {
            view.setOnClickListener(this)
            view.setCompanyName(this.companyName)
            view.setCompanyType(this.companyType)
            view.setCompanyCountry(this.companyCountry)
        }

    }

    override fun notifyAdapter(companyQuery: String) {
        val companyList = ArrayList<Company>()
        for (company in companiesCache) {
            if (company.companyName.toLowerCase().contains(companyQuery.toLowerCase())) {
                companyList.add(company)
            } else {
                companyList.remove(company)
            }
        }
        setCompanyRecycler(companyList)
    }

    private fun getCompanies(response: JsonObject): List<CompanyResponse>{

        val companies = response.get("enterprises").asJsonArray

        return Gson().fromJson<List<CompanyResponse>>(companies, object: TypeToken<List<CompanyResponse>>(){}.type)

    }

    private fun setCompanyRecycler(data: List<Company>){
        companiesList = (data)
        companyView.showCompanies()
    }

}
