package com.example.thiago.empresas_android.domain.entities

import java.io.Serializable

data class Company(
      val companyName: String,
      val companyCountry : String,
      val companyType : String,
      val companyDescription : String
) : Serializable