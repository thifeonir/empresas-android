package com.example.thiago.empresas_android.Login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.example.thiago.empresas_android.Company.CompanyActivity
import com.example.thiago.empresas_android.R
import com.example.thiago.empresas_android.data.datastores.remote.entities.UserResponse
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity: AppCompatActivity(), LoginContract.View{

    private val loginPresenter : LoginContract.Presenter = LoginPresenter()
    private val sharedPreferences by lazy {
        this.getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginPresenter.attachView(this)
    }

    override fun onStart() {
        super.onStart()
        loginTextInputEditText.setText("testeapple@ioasys.com.br")
        passwordTextInputEditText.setText("12341234")
        button_home.setOnClickListener {
            validateLogin(loginTextInputEditText.text.toString(),
                    passwordTextInputEditText.text.toString())
        }
    }

    override fun validateLogin(email : String, password : String) {
        loginPresenter.doLogin(email, password)
        showLoading()
        hideLoginButton()
    }

    override fun showLoading() {
        login_progressbar.visibility = VISIBLE
    }

    override fun hideLoading() {
        login_progressbar.visibility = GONE
    }

    override fun showToast(message : String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun startCompanyActivity() {
        var companyIntent = Intent(this, CompanyActivity::class.java)
        startActivity(companyIntent)
        finish()
    }

    override fun showLoginButton() {
        button_home.visibility = VISIBLE
    }

    override fun hideLoginButton() {
        button_home.visibility = GONE
    }

    override fun saveUserInfo(userInfo : UserResponse) {
        val editor = sharedPreferences.edit()
        editor.putString("userInfo", Gson().toJson(userInfo))
        editor.apply()
    }
}