package com.example.thiago.empresas_android.presentation.Company

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.thiago.empresas_android.Company.CompanyContract
import com.example.thiago.empresas_android.CompanyDetail.CompanyDetailActivity
import com.example.thiago.empresas_android.R
import com.example.thiago.empresas_android.domain.entities.Company

class CompanyAdapter(presenter : CompanyContract.Presenter) : RecyclerView.Adapter<CompanyAdapter.CompanyViewHolder>() {

    private val recyclerPresenter: CompanyContract.Presenter = presenter


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyViewHolder {

        return CompanyViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.card_company, parent, false))
    }

    override fun getItemCount(): Int {
        var size = recyclerPresenter.getCompanyRowCount()
        Log.e("Size", size.toString())
        return size
    }

    override fun onBindViewHolder(holder: CompanyViewHolder, position: Int) {

        recyclerPresenter.onBindCompanyRowViewAtPosition(position, holder)
    }

    inner class CompanyViewHolder(view: View) : RecyclerView.ViewHolder(view), CompanyCardView {

        private val companyNameTextView: TextView = view.findViewById(R.id.company_card_tv_name)
        private val companyTypeTextView: TextView = view.findViewById(R.id.company_card_type_tv)
        private val companyCountryTextView: TextView = view.findViewById(R.id.company_country_tv)
        private val companyImageView: ImageView = view.findViewById(R.id.company_card_image)
        private val myView: View = view
        private val EXTRA_COMPANY_NAME = "COMPANY_NAME"

        init {
            setCompanyImage()
        }

        override fun setCompanyName(companyName: String) {
            companyNameTextView.text = companyName
        }

        override fun setCompanyCountry(companyCountry: String) {
            companyCountryTextView.text = companyCountry
        }

        override fun setCompanyDescription(companyDescription: String) {

        }

        override fun setCompanyType(companyType: String) {
            companyTypeTextView.text = companyType
        }

        private fun setCompanyImage(){
            companyImageView.setImageResource(R.drawable.img_e_1)
        }

        override fun setOnClickListener(company: Company) {

            myView.setOnClickListener {
                Log.d("OnClick", "onClick: clicked on: ")

                Toast.makeText(myView.context, company.companyName, Toast.LENGTH_SHORT).show()

                fireIntent(myView.context, company)

            }
        }

        private fun fireIntent(context: Context, company: Company) {

            val intent = Intent(context, CompanyDetailActivity::class.java)
            intent.putExtra(EXTRA_COMPANY_NAME, company.companyName)

            context.startActivity(intent)
        }

    }
}
