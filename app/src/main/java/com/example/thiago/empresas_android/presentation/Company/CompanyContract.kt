package com.example.thiago.empresas_android.Company

import com.example.thiago.empresas_android.data.datastores.remote.entities.UserResponse
import com.example.thiago.empresas_android.presentation.Company.CompanyCardView

interface CompanyContract {

    interface View {
        fun showCompanies()

        fun onDataChanged()

        fun showLoading()

        fun hideLoading()
    }

    interface Presenter{

        fun attachView(view : View)

        fun loadCompanies(userInfo : UserResponse)

        fun onBindCompanyRowViewAtPosition(position : Int, view : CompanyCardView)

        fun notifyAdapter(companyQuery: String)

        fun getCompanyRowCount() : Int

        fun notifyDataChanged()
    }
}