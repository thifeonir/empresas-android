package com.example.thiago.empresas_android.data.datastores.remote

import com.example.thiago.empresas_android.data.datastores.remote.interfaces.ServiceApiDef
import com.example.thiago.empresas_android.data.datastores.remote.utils.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ServiceApi {
    companion object {
        private fun getOkHttpClient(): OkHttpClient {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            return OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build()
        }

        private fun provideRetrofit(): Retrofit {
            val httpClient = getOkHttpClient()

            return Retrofit.Builder()
                    .client(httpClient)
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(httpClient)
                    .build()
        }

        fun provideApiService(): ServiceApiDef {
            return provideRetrofit()
                    .create(ServiceApiDef::class.java)
        }
    }
}