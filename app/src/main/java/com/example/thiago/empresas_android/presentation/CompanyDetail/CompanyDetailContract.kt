package com.example.thiago.empresas_android.CompanyDetail

interface CompanyDetailContract {

    interface View{
        fun showCompanyDetail(companyName: String, companyDescription: String)

        fun showLoading()

        fun hideLoading()

        fun hideTitle()

        fun showTitle(title: String)

        fun hideDescription()

        fun showDescription(description: String)

        fun setImage()

        fun showToast(message: String)
    }

    interface Presenter{

        fun attachView(view: CompanyDetailContract.View)

        fun loadCompanyDetail(companyName: String)
    }
}

