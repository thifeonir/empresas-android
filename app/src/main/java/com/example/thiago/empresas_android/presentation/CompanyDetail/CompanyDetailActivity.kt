package com.example.thiago.empresas_android.CompanyDetail

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import com.example.thiago.empresas_android.R
import com.example.thiago.empresas_android.data.datastores.remote.entities.UserResponse
import com.example.thiago.empresas_android.presentation.CompanyDetail.CompanyDetailPresenter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_company_detail.*


class CompanyDetailActivity : AppCompatActivity(), CompanyDetailContract.View {

    lateinit var presenter: CompanyDetailContract.Presenter
    private val sharedPreferences by lazy {
        this.getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_detail)
        setSupportActionBar(detailToolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        presenter = CompanyDetailPresenter(companyName = intent.getStringExtra(EXTRA_COMPANY_NAME), view = this, userInfo = loadUserInfo())
    }

    override fun showCompanyDetail(companyName: String, companyDescription: String) {
        hideLoading()
        detail_layout.visibility = VISIBLE
        showTitle(companyName)
        showDescription(companyDescription)

    }

    override fun showLoading() {
        detail_progressbar.visibility = VISIBLE
    }

    override fun hideLoading() {
        detail_progressbar.visibility = GONE
    }

    override fun hideTitle() {
        company_detail_name_tv.visibility = GONE
    }

    override fun showTitle(title: String) {
        company_detail_name_tv.visibility = VISIBLE
        company_detail_name_tv.text = title
    }

    override fun hideDescription() {
        company_detail_description.visibility = GONE
    }

    override fun showDescription(description: String) {
        company_detail_description.visibility = VISIBLE
        company_detail_description.text = description
    }

    override fun setImage() {
        company_detail_image.setImageResource(R.drawable.img_e_1)
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        } else {
            super.onOptionsItemSelected(item)
            return false
        }
    }

    private fun loadUserInfo() : UserResponse = Gson().fromJson<UserResponse>(sharedPreferences.getString("userInfo", null), UserResponse::class.java)

    companion object {
        const val EXTRA_COMPANY_NAME = "COMPANY_NAME"
    }
}

