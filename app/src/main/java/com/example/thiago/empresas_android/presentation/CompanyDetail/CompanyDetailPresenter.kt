package com.example.thiago.empresas_android.presentation.CompanyDetail

import android.util.Log
import com.example.thiago.empresas_android.CompanyDetail.CompanyDetailContract
import com.example.thiago.empresas_android.data.datastores.remote.ServiceApi
import com.example.thiago.empresas_android.data.datastores.remote.entities.CompanyResponse
import com.example.thiago.empresas_android.data.datastores.remote.entities.UserResponse
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class CompanyDetailPresenter(companyName: String, view: CompanyDetailContract.View, private val userInfo: UserResponse) : CompanyDetailContract.Presenter {

    private lateinit var myView: CompanyDetailContract.View
    private val serviceApi = ServiceApi

    init {
        attachView(view)
        loadCompanyDetail(companyName)
    }

    override fun attachView(view: CompanyDetailContract.View) {
        myView = view
    }

    override fun loadCompanyDetail(companyName: String) {

        serviceApi.provideApiService()
                .getCompanyByName(userInfo.uid,
                userInfo.accessToken,
                userInfo.client, companyName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map {response: JsonObject -> getCompany(response)}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    setupDetailActivity(it[0])
                },{
                    Log.d("Errorrr", it.localizedMessage)
                },{
                    Log.d("Completed", "complete")
                })
    }

    private fun setupDetailActivity(company: CompanyResponse) {
        myView.showCompanyDetail(company.name, company.description)
    }

    private fun getCompany(response: JsonObject): List<CompanyResponse>{

        val companies = response.get("enterprises").asJsonArray

        return Gson().fromJson<List<CompanyResponse>>(companies, object: TypeToken<List<CompanyResponse>>(){}.type)

    }
}