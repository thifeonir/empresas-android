package com.example.thiago.empresas_android.Company

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import com.example.thiago.empresas_android.R
import com.example.thiago.empresas_android.data.datastores.remote.entities.UserResponse
import com.example.thiago.empresas_android.presentation.Company.CompanyAdapter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_company.*

class CompanyActivity : AppCompatActivity(), CompanyContract.View {

    private val companyPresenter : CompanyContract.Presenter = CompanyPresenter()
    private lateinit var companyAdapter : CompanyAdapter
    private lateinit var companyLayoutManager : LinearLayoutManager
    private val sharedPreferences by lazy {
        this.getSharedPreferences("sharedPref", Context.MODE_PRIVATE)
    }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_company)
            setSupportActionBar(companyToolbar)
            supportActionBar?.setDisplayShowTitleEnabled(false)

            companyPresenter.attachView(this)
        }

        override fun onCreateOptionsMenu(menu: Menu): Boolean {
            super.onCreateOptionsMenu(menu)

            val inflater = menuInflater
            inflater.inflate(R.menu.search_menu, menu)
            val mSearchView = menu.findItem(R.id.search).actionView as SearchView
            mSearchView.queryHint = "Pesquisar"
            mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

                override fun onQueryTextChange(newText: String): Boolean {
                    companyPresenter.notifyAdapter(newText)
                    return false
                }

                override fun onQueryTextSubmit(query: String): Boolean {
                    companyPresenter.notifyAdapter(query)
                    return false
                }

            })

            val searchAutoComplete = mSearchView.findViewById<SearchView.SearchAutoComplete>(android.support.v7.appcompat.R.id.search_src_text)
            searchAutoComplete.setHintTextColor(Color.WHITE)
            searchAutoComplete.setTextColor(Color.WHITE)

            val searchCloseIcon = mSearchView.findViewById<ImageView>(android.support.v7.appcompat.R.id.search_close_btn)
            searchCloseIcon.setImageResource(R.drawable.ic_clear_search_24dp)

            MenuItemCompat.setOnActionExpandListener(menu.findItem(R.id.search), object : MenuItemCompat.OnActionExpandListener {
                override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                    showLoading()
                    tv_tip.visibility = View.GONE
                    toolbar_logo_image.visibility = View.GONE
                    companyPresenter.loadCompanies(loadUserInfo())
                    return true
                }

                override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                    company_recycler_view.visibility = GONE
                    toolbar_logo_image.visibility = View.VISIBLE
                    tv_tip.visibility = View.VISIBLE
                    return true
                }
            })

            return true
        }

        override fun showCompanies() {
            hideLoading()
            company_recycler_view.visibility = VISIBLE
            companyAdapter = CompanyAdapter(companyPresenter)
            companyLayoutManager = LinearLayoutManager(this)

            company_recycler_view.adapter = companyAdapter
            company_recycler_view.layoutManager = companyLayoutManager
        }

    override fun showLoading() {
        company_progressbar.visibility = VISIBLE
    }

    override fun hideLoading() {
        company_progressbar.visibility = GONE
    }

        override fun onDataChanged() {
            companyAdapter.notifyDataSetChanged()
        }

        fun loadUserInfo() : UserResponse = Gson().fromJson<UserResponse>(sharedPreferences.getString("userInfo", null), UserResponse::class.java)
}