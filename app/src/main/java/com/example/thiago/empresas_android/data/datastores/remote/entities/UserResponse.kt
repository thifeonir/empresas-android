package com.example.thiago.empresas_android.data.datastores.remote.entities

import com.squareup.moshi.Json

class UserResponse (@Json(name = "uid") val uid : String,
                    @Json(name = "client") val client : String,
                    @Json(name = "access-token") val accessToken : String)