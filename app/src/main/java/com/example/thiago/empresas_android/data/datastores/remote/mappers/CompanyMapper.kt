package com.example.thiago.empresas_android.data.datastores.remote.mappers

import com.example.thiago.empresas_android.data.datastores.remote.entities.CompanyResponse
import com.example.thiago.empresas_android.domain.entities.Company
import com.example.thiago.empresas_android.domain.mappers.Mapper

class CompanyMapper: Mapper<CompanyResponse, Company>() {
    override fun transform(value: CompanyResponse): Company {
        return Company(
                companyName = value.name,
                companyType = value.enterpriseType.enterpriseTypeName,
                companyDescription = value.description,
                companyCountry = value.country
        )
    }

}