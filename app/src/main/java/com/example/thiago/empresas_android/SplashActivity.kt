package com.example.thiago.empresas_android

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.thiago.empresas_android.Login.LoginActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var loginIntent = Intent(this, LoginActivity::class.java)
        startActivity(loginIntent)
        finish()
    }
}
