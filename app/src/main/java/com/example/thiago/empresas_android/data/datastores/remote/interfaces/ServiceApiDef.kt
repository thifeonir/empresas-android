package com.example.thiago.empresas_android.data.datastores.remote.interfaces

import com.example.thiago.empresas_android.data.datastores.remote.entities.UserResponse
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.*
import rx.Observable
import rx.Single

interface ServiceApiDef {

    @FormUrlEncoded
    @POST("api/v1/users/auth/sign_in")
    fun doLogin(@Field("email") email: String, @Field("password") password: String): Single<Response<UserResponse>>

    @GET("/api/v1/enterprises")
    fun getCompanies(@Header("uid") uid: String, @Header("access-token") accessToken: String,
                     @Header("client") client: String): Observable<JsonObject>

    @GET("/api/v1/enterprises")
    fun getCompanyByName(@Header("uid") uid: String, @Header("access-token") accessToken: String,
                         @Header("client") client: String, @Query("name") companyName: String): Observable<JsonObject>

}