package com.example.thiago.empresas_android.domain.entities

import java.io.Serializable

data class User (
        val uid : String,
        val client : String,
        val accessToken : String
) : Serializable