package com.example.thiago.empresas_android.Login

import android.content.Context.MODE_PRIVATE
import com.example.thiago.empresas_android.data.datastores.remote.ServiceApi
import com.example.thiago.empresas_android.data.datastores.remote.entities.UserResponse
import com.example.thiago.empresas_android.data.datastores.remote.interfaces.ServiceApiDef
import retrofit2.Response
import rx.SingleSubscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class LoginPresenter : LoginContract.Presenter {

    private lateinit var loginView : LoginContract.View
    private val serviceApi = ServiceApi

    override fun attachView(view: LoginContract.View) {
        loginView = view
    }

    override fun doLogin(email: String, password: String) {
        serviceApi.provideApiService()
                .doLogin(email, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : SingleSubscriber<Response<UserResponse>>() {
                    override fun onSuccess(userInfoResponse: Response<UserResponse>) {
                        if (userInfoResponse.code() == 200) {
                            val userInfo = UserResponse(userInfoResponse.headers().get("uid").toString(),
                                    userInfoResponse.headers().get("client").toString(),
                                    userInfoResponse.headers().get("access-token").toString())
                            loginView.startCompanyActivity()
                            loginView.showToast("Bem vindo "+userInfo.uid)
                            loginView.saveUserInfo(userInfo)

                        } else {
                            loginView.showToast("Não foi possivel fazer login!")
                        }
                    }

                    override fun onError(error: Throwable) {
                        loginView.showToast("Usuário não encontrado, tente novamente")
                    }
                })

    }
}
